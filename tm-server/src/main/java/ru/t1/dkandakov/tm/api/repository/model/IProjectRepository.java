package ru.t1.dkandakov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.enumerated.ProjectSort;
import ru.t1.dkandakov.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

    @Nullable
    List<Project> findAll(@NotNull String userId, @NotNull ProjectSort sort);

}
