package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ITokenService {

    @Nullable
    String getToken();

    void setToken(@Nullable String token);

}