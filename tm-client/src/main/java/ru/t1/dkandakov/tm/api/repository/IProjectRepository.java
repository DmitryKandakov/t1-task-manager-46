package ru.t1.dkandakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;

public interface IProjectRepository extends IUserOwnedRepository<ProjectDTO> {

    @NotNull
    ProjectDTO create(@Nullable String userId, @NotNull String name, @NotNull String description);

    @NotNull
    ProjectDTO create(@Nullable String userId, @NotNull String name);

}